// global variables to be changed during function call
var bedtime = new Date();
var wakeuptime;
var diff = 0;
$(document).ready(function() {
    // access button with id "sleep"
    $("#sleep").on("click",function() {
        var clicked_obj = $(this);
        // log that something was clicked
        console.log(clicked_obj);
        $.ajax({
            // defining where to send the response to, and in the json format
            url: '/respond',
            type: 'POST',
            data: JSON.stringify({ response: $(this).text() }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",

            success: function(response){
                // now log the text of the button that was clicked
                // it will swap between Morning and night
                console.log(response);

                // test if the button text is good night
                test = response.response.toString();
                x ="Good Night!".localeCompare(test);

                let mornnight;

                // if it is good night, set it to good morning, start the timer and clear the hours slept
                if (x===0) {
                    mornnight = "Good Morning!";
                    bedtime = new Date;
                    diff=0;
                }
                // if it is good morning, calculate the time slept
                else {
                    mornnight = "Good Night!";
                    wakeuptime = new Date;
                    diff = Math.abs(wakeuptime - bedtime)/36e5;
                    bedtime = 0;
                }
                // change the hours slept and the button text
                diff = diff.toFixed(2);
                    $("#hourslept").text(diff + "hours");
                    $("#sleep").text(mornnight);
            },
            error: function(error){
                console.log(error);
            }
        });


    });
});
