from flask_wtf import FlaskForm
from wtforms import StringField, DateField, IntegerField, BooleanField
from wtforms.validators import DataRequired

# Form to provide the function of adding a new task
class task(FlaskForm):
    Module = StringField('Module', validators=[DataRequired()])
    Coursework_Number = IntegerField('Coursework Number', validators=[DataRequired()])
    Description = StringField('Description', validators=[DataRequired()])
    Date_Due = StringField('Date Due', validators=[DataRequired()])
