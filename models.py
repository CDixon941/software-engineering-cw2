from app import db


class CW(db.Model):
    Module = db.Column(db.String(500))
    Coursework_Number = db.Column(db.Integer)
    Description = db.Column(db.String(500), primary_key=True, unique=True)
    Date_Due = db.Column(db.String(500))
    Completed = db.Column(db.Boolean)
