# flask dependencies
from flask import Flask, render_template, request, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_restful import Api
import os

# python dependencies
import json

# initialise the app and the database
app = Flask(__name__)
api = Api(app)
app.config.from_object('config')
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'app.db')
db = SQLAlchemy(app)
db.create_all()
migrate = Migrate(app, db)

from models import CW
from form import task

data = CW.query.all()


@app.route('/', methods=['GET', 'POST'])
def hello_world():
    data = CW.query.all()
    newtask = task()
    if newtask.validate_on_submit():
        print("hello")
        x = CW(Module=newtask.Module.data, Coursework_Number=newtask.Coursework_Number.data,
               Description=newtask.Description.data, Date_Due=newtask.Date_Due.data,Completed=False)
        db.session.add(x)
        db.session.commit()
        newdata = CW.query.all()
        return render_template("index.html", morn_night="Good Morning", form=newtask, data=newdata)
    return render_template("index.html", morn_night="Good Morning", form=newtask, data=data)


@app.route('/respond', methods=['POST'])
def respond():
    # Parse the JSON data included in the request
    data = json.loads(request.data)
    response = data.get('response')
    # DO STUFF... Process the response here

    # Return a request to the JavaScript
    return json.dumps({'status': 'OK', 'response': response})


if __name__ == '__main__':
    app.run()
